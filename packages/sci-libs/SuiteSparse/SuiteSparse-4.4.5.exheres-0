# Copyright 2015 Thomas G. Anderson
# Distributed under the terms of the GNU General Public License v2

WORK="${WORKBASE}"/${PN}

SUMMARY="A suite of sparse matrix algorithms"
DESCRIPTION="Algorithms include:

* UMFPACK: multifrontal LU factorization.
* CHOLMOD: supernodal Cholesky.
* SPQR: multifrontal QR.
* KLU and BTF:  sparse LU factorization.
* Ordering methods (AMD, CAMD, COLAMD, and CCOLAMD).
* CSparse and CXSparse: a concise sparse Cholesky factorization package.
* SSMULT and SFMULT: sparse matrix multiplication.
"
HOMEPAGE="http://faculty.cse.tamu.edu/davis/suitesparse.html"
DOWNLOADS="http://faculty.cse.tamu.edu/davis/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        virtual/blas
        virtual/lapack
"

DEFAULT_SRC_PREPARE_PATCHES=( -p2 "${FILES}"/${PN}-4.4.4-shared-lib.patch )
DEFAULT_SRC_COMPILE_PARAMS=(
    "-j1"
    BLAS="-lblas"
    LAPACK="-llapack"
    RANLIB="$(exhost --tool-prefix)ranlib"
    library
)
DEFAULT_SRC_INSTALL_PARAMS=(
    INSTALL_INCLUDE="${IMAGE}/usr/$(exhost --target)/include"
    INSTALL_LIB="${IMAGE}/usr/$(exhost --target)/lib"
)

src_install() {
    edo mkdir -p "${IMAGE}"/usr/$(exhost --target)/{include,lib}
    default
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/*.a
}

