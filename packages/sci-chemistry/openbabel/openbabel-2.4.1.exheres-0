# Copyright 2009 Ingmar Vanhassel
# Copyright 2010 Bo Ørsted Andresen
# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=${PNV//./-} ] cmake

SUMMARY="A chemical toolbox designed to speak the many languages of chemical data"
DESCRIPTION="
Open Babel is a community-driven scientific project including both
cross-platform programs and a developer library designed to support molecular
modeling, chemistry, and many related areas, including interconversion of file
formats and data.
"
HOMEPAGE="http://openbabel.org/ ${HOMEPAGE}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc
    gui    [[ description = [ Build a GUI using wxWidgets ] ]]
    java   [[ description = [ Adds bindings for Java ] ]]
    openmp [[ description = [ Enable parallel processing using OpenMP ] ]]
    R      [[ description = [ Adds bindings for R ] ]]
    perl python"

RESTRICT="test" # Tests are still utterly broken (2.3.2)

DEPENDENCIES="
    build:
        sci-libs/eigen:3[>=2.91.0]
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/libxml2:2.0
        sci-chemistry/inchi
        sys-libs/zlib
        x11-libs/cairo   [[ note = [ PNG output ] ]]
        gui? ( x11-libs/wxGTK:= )
        java? (
            dev-lang/swig[>=2.0]
            virtual/jdk:=   [[ note  = [ Java Native Interface ] ]]
        )
        openmp? ( sys-libs/libgomp:= )
        perl? (
            dev-lang/perl:=
            dev-lang/swig[>=2.0]
        )
        python? (
            dev-lang/python:=
            dev-lang/swig[>=2.0]
        )
        R? (
            dev-lang/swig[>=2.0]
            sci-lang/R
        )
"

UPSTREAM_RELEASE_NOTES="http://openbabel.org/wiki/Open_Babel_${PV}"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Compiler-version-parsing-and-comparison-from-CMake-2.patch
)

src_configure() {
    local cmakeparams=(
        -DBABEL_DATAROOTDIR:PATH=/usr/share
        -DBINDINGS_ONLY:BOOL=FALSE
        -DBUILD_EXAMPLES:BOOL=FALSE
        -DBUILD_SHARED:BOOL=TRUE
        -DCSHARP_BINDINGS:BOOL=FALSE
        # We need this atm because in a previous version libinchi was bundled.
        # So there is still another version of libinchi installed before openbabel
        # is upgraded and we need to make sure to build against the right one.
        -DINCHI_INCLUDE_DIR=/usr/$(exhost --target)/inchi
        -DINCHI_LIBRARY=/usr/$(exhost --target)/lib/libinchi.so.1
        -DOPENBABEL_USE_SYSTEM_INCHI:BOOL=TRUE
        -DPHP_BINDINGS:BOOL=FALSE
        # Ruby bindings currently fail to build with ruby:2.2
        -DRUBY_BINDINGS:BOOL=FALSE
        -DWITH_INCHI:BOOL=TRUE
        $(cmake_build doc DOCS)
        $(cmake_build GUI)
        $(cmake_enable OPENMP)
        $(cmake_disable_find gui wxWidgets)
        $(cmake_option java JAVA_BINDINGS)
        $(cmake_option perl PERL_BINDINGS)
        $(cmake_option python PYTHON_BINDINGS)
        $(cmake_option R R_BINDINGS)
        $(expecting_tests -DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE)
    )

    if option java || option perl || option python ; then
        cmakeparams+=( -DRUN_SWIG:BOOL=TRUE )
    fi

    ecmake "${cmakeparams[@]}"
}

src_compile() {
    default

    option doc && emake docs
}

src_install() {
    cmake_src_install

    if option doc; then
        edo pushd "${CMAKE_SOURCE}"/doc/API/
        insinto /usr/share/doc/${PNVR}
        doins -r html
        edo popd
    fi
}

