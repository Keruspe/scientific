# Copyright 2015 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KiCad ]
require cmake [ api=2 ]

SUMMARY="Documentation for the KiCad EDA suite"
HOMEPAGE+=" http://www.kicad-pcb.org/"

LICENCES="|| ( GPL-3 CCPL-Attribution-3.0 )"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc[>=8.6.9]
        app-text/dblatex[>=0.3.4]
        app-text/po4a[>=0.45]
        app-text/source-highlight
        sys-devel/gettext[>=0.18]
    build+run:
        !sci-electronics/kicad[<4.0.0] [[
            description = [ kicad-doc used to be installed along with kicad ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # It's only possible to build either a single language or all available
    # languages. Since the latter would include japanese, which needs additional
    # LaTeX packages, we'll just build english for now
    -DSINGLE_LANGUAGE:STRING=en
)

